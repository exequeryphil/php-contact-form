
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$('form').on('submit', function(e) {

    e.preventDefault();
    let formData = $(this).serialize();

    axios.post('/contact', formData)
        .then(function (response) {

            $('#alert').removeClass()
            $('#alert').addClass('alert alert-success')
            $('#alert').text('Thanks! Guy will be in touch shortly!')
            $('form')[0].reset()

        })
        .catch(function (error) {

            if (error.response.status == 422) {
                let errors = error.response.data.errors
                let firstError = errors[Object.keys(errors)[0]][0]
                $('#alert').removeClass()
                $('#alert').addClass('alert alert-danger')
                $('#alert').text(firstError)
            } else {
                $('#alert').removeClass()
                $('#alert').addClass('alert alert-warning')
                $('#alert').text('Sorry, please try again later')
            }

        });

});