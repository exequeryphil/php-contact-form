## Dev Notes

This was built with Laravel 5.5, so:
  
1.  Probably best to run ```composer install``` before trying to run locally.
2.  Dev environment uses MySQL for storage, so run ```artisan migrate``` before manually submitting a form.
3.  PHPUnit uses SQLite in memory, so no db required there.
4.  BTW, made with Homestead
