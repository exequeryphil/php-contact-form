<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Mail\Contact;


class ContactController extends Controller
{

    /**
     * Evaluate and submit form data
     *
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function submit (Request $request) {

        $messages = [
            'email.required' => 'Please enter a valid email address',
            'email.email' => 'Please enter a valid email address',
            'fullname.required' => 'Please enter a valid name',
            'message.required' => 'Please enter a valid message',
        ];

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'fullname' => 'required|min:1|max:255',
            'message' => 'required|min:2|max:1000',
            'phone' => 'max:50'
        ], $messages);

        if ($validator->fails())  {
            return response()->json(['errors'=>$validator->errors()], 422);
        }

        \Mail::to('guy-smiley@example.com')->send(new Contact($request));
        \App\Contact::firstOrCreate($request->toArray());

        return response()->json(['success' => true]);

    }

}
