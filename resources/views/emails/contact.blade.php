@component('mail::message')
# Hi Guy,

Someone named {{ $data->fullname }} sent you this message:

{{ $data->message }}

Please reply at {{ $data->email }} @if(!empty($data->phone)) or call at {{ $data->phone }} @endif


Thanks,<br>
Your Website
@endcomponent
