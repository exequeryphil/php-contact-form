<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Mail\Contact;


class ContactTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * Checks if homepage loads with HTML form
     *
     * @return void
     */
    public function test_proper_homepage_load () {

        $this->get('/')
            ->assertStatus(200)
            ->assertSee('Submit');

    }

    /**
     * Ensures default response to endpoint is 422
     *
     * @return void
     */
    public function test_default_endpoint_status () {

        $response = $this->post('/contact');
        $response->assertStatus(422);

        $response = $this->get('/contact');
        $response->assertStatus(404);

    }

    /**
     * Tests error response on submission without required fields
     *
     * @return void
     */
    public function test_submission_without_required_fields() {

        \Mail::fake();
        $response = $this->withHeaders([
            'X-CSRF-TOKEN' => csrf_token(),
        ])->json('POST', '/contact', [
            'fullname' => '',
            'email' => '',
            'message' => '',
            'phone' => '111-222-3333'
        ]);
        $response->assertStatus(422)
            ->assertJson(['errors' => true])
            ->assertJsonCount(3, 'errors');
        \Mail::assertNotSent(Contact::class);

    }

    /**
     * Tests error response when values longer than max length
     *
     * @return void
     */
    public function test_submission_with_long_values () {

        \Mail::fake();
        $response = $this->withHeaders([
        'X-CSRF-TOKEN' => csrf_token(),
        ])->json('POST', '/contact', [
            'fullname' => str_repeat('x', 256),
            'email' => 'dan@danger.com',
            'message' => str_repeat('x', 1001),
            'phone' => str_repeat('x', 51),
        ]);
        $response->assertStatus(422)
            ->assertJson(['errors' => true])
            ->assertJsonCount(3, 'errors');
        \Mail::assertNotSent(Contact::class);

    }

    /**
     * Tests response to invalid email submission
     *
     * @return void
     */
    public function test_submission_with_invalid_email () {

        \Mail::fake();
        $response = $this->withHeaders([
            'X-CSRF-TOKEN' => csrf_token(),
        ])->json('POST', '/contact', [
            'fullname' => 'Dan Danger',
            'email' => 'dan @ danger . dan',
            'message' => 'Remember me?',
            'phone' => ''
        ]);
        $response->assertStatus(422)
            ->assertJson(['errors' => true])
            ->assertJsonCount(1, 'errors');
        \Mail::assertNotSent(Contact::class);

    }

    /**
     * Tests response to valid submission
     *
     * @return void
     */
    public function test_valid_submission () {

        \Mail::fake();
        $response = $this->withHeaders([
            'X-CSRF-TOKEN' => csrf_token(),
        ])->json('POST', '/contact', [
            'fullname' => 'Dan Doe',
            'email' => 'dan@doe.com',
            'message' => 'Remember me?',
            'phone' => '111-222-3333'
        ]);
        $response->assertStatus(200)->assertJson(['success' => true]);
        $this->assertDatabaseHas('contacts', [
            'fullname' => 'Dan Doe',
            'email' => 'dan@doe.com',
            'message' => 'Remember me?',
            'phone' => '111-222-3333'
        ]);
        \Mail::assertSent(Contact::class, function ($mail) {
            return $mail->hasTo('guy-smiley@example.com');
        });

    }


}
